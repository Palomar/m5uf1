## 1.4 Paradigmas de programación

Ante la variedad de lenguajes de programación resulta difícil hacer una clasificación general, así que siempre toman características de la clasificación siguiente como paradigmas:

* Paradigma imperativo/estructurado.
* Paradigma de objetos.
* Paradigma funcional.
* Paradigma lógico.

### Paradigma imperativo/estructurado

Responde a aquel paradigma en que sus sentencias son imperativas, es decir sus sentencias dan órdenes que alteran los datos. Su técnica es la **programación estructurada**:

* Secuencias.
* Selección.
* Iteraciones.

A su vez se da la técnica del diseño descendente (_top-down_) por medio del cual se programa de forma modular generando cada vez funciones más pequeñas y específicas, a menudo reutilizables.

### Paradigma de objetos

También conocido como **programación orientada a objetos** (POO) se basa en la abstracció del mundo real. En este caso el proceso ya no es crear funciones o procedimientos, sino objetos que son representaciones directas de cosas del mundo real.
Un *objeto* es una combinación de datos (atributos) y métodos (funciones y procedimientos) con los se puede interactuar. Con este tipo de programación los ojetos interactúan entre sí por medio de las llamadas a sus métodos.

La programación de este paradigma se basa en conceptos que se deben seguir rigurosamente:

* Abstracción.
* Encapsulación.
* Modularidad.
* Jerarquía.
* Polimorfismo.

### Paradigma funcional

Se basan en el modelo matemático y consisten en que el resultado de un cálculo es la entrada del siguiente, así hasta producir una composición con el resultado deseado.
Son para el ámbito de la investigación científica y matemática principalmente.

### Paradigma lógico

Se basa en la aplicación de reglas lógicas para inferir conclusiones a partir de datos. Sobre dicha base de conocimientos se realizan las consultas.
La base de conocimientos la forman hechos que se encuentran relacionados entre datos y las reglas lógicas, de forma que de la combinación de estos se obtienen consecuencias sobre las que consultar.

Este paradigma lógico se emplea en lenguajes destinados a la Inteligencia Artificial y a los sistemas expertos. También ante aquellos problemas de combinatoria que requieren de un gran espectro de soluciones alternativas.