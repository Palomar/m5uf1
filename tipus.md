## 1.3 Tipos de lenguajes de programación.

Un **lenguaje de programación** es el lenguaje que permite al usuario establecer una comunicación con la máquina por medio del código fuente que posteriormente se compilará y enlazará en un código ejecutable.

Los lenguajes de programación son de los siguientes tipos:

* De primera generación o lenguaje máquina.
* De segunda generación o lenguaje ensamblador.
* De tercera generación o lenguaje de alto nivel.
* De cuarta generación o lenguaje de propósito específico.
* Lenguaje quinta generación.

### Lenguaje máquina

Así pues el primero que se desarrolló fue el de primera generación o **lenguaje máquina**, el cual es el único que entiende la máquina directamente.
Dicho lenguaje se adapta a la arquitectura del procesador lo que implica instrucciones en código binario que hacen altamente complicado para un humano programar algo empleando dichas instrucciones.
Sin embargo, al tener acceso directo a la máquina se permite un control sobre ésta enorme por lo que se pueden obtener resultados muchísimo más eficientes.

### Lenguaje ensamblador

El segundo en desarrollarse fue el **lenguaje ensamblador** que se caracteriza por ser el primero en emplear instrucciones mucho más fáciles de recordar para el ser humano por medio letras.
Dichas operaciones son una abstracción del conocimiento de la máquina de forma que ésas tienen una equivalente instrucción en lenguaje de la máquina. Por tanto es un lenguaje que el uso de sus instrucciones depende totalmente de la máquina.
Por lo general se usa en la actualidad para controladores o _drivers_ que requieren de uso en tiempo real con el fin de ser eficientes en velocidad y memoria. Lo que no quita tampoco que si alguien se propone hacer una aplicación visual como un editor de imágenes o un videojuego no pueda hacerlo, sin embargo requiere de un mayor esfuerzo para el programador desarrollarlo habiendo otras y mejores opciones.

Tanto el de primera como el de segunda generación, permiten al programador:

* Una optimización del programa sobre la máquina.
* Especificar exactamente qué instrucciones usar.

Sin embargo:

* Son difíciles de trasladar a otras máquinas al estar ligados a la máquina para la que fueron programados.
* Requieren de conocimientos muy amplios sobre la arquitectura de la máquina.
* No permiten expresar algoritmos de uso habitual como estructuras de control, se deben crear.
* Difíciles de codificar, mantener y documentar.

### Lenguaje de alto nivel

Estos lenguajes son una evolución que permite emplear palabras o frases fáciles de comprender y facilitan el control del flujo en la programación como son las estructuras de control, bucles, etc.
Se usan para desarrollar aplicaciones complejas y grandes ya que deben ser fáciles de entender para el humano a costa de reducir el rendimiento en memoria o velocidad.

Los **lenguajes de alto nivel** son fáciles de aprender y son más naturales para el lenguaje humano, tomando generalmente el uso de palabras como el inglés.
Estos lenguajes necesitan entonces de los compiladores y el enlazador para ayudar a traducir el lenguaje de alto nivel al lenguaje máquina que pertoque, por lo que el compilador y enlazador también variará.

Por otro lado existen otros lenguajes de alto nivel como los que requieren de **intérpretes** que traduce el código fuente a _código de bytes_. A diferencia del compilador, el intérprete lo hace en tiempo de ejecución, por lo que no hay un proceso previo de traducción antes de ejecutarlo todo, se hace instrucción a instrucción.

Destaca por:

* No se ejecuta por el sistema sino a través del intérprete.
* Cada sistema tiene su propio intérprete.
* El lenguaje interpretado es más lento que el compilado al hacerse siempre en cada instrucción y siempre que se ejecute.
* Un programa en dicho lenguaje es más porteable a diferentes sistemas, mientras que en uno compilado no funcionará y requiere de otra compilación distinta.

Características:

* El código del programa es senzillo y comprensible.
* Independientes de la máquina, permite ser porteado.
* Es más fácil de escribir y de mantener.
* Su ejecución puede ser más lenta que los de primera y segunda generación, dependerá de la traducción del compilador.

### Lenguajes de propósito específico

Son lenguajes de un nivel de abstracción superior permitiendo desarrollar en menor espacio de tiempo y esfuerzo lo que se hace en lenguajes de tercera generación.
Por tanto se automatizan muchos aspectos que antes se debían hacer a mano, empleando pocas líneas de código o directamente ninguna.
Permiten prototipus de aplicaciones rápidamente antes de tener el programa terminado.

Características:

* Mayor abstracción.
* Menor esfuerzo de programación.
* Menor coste de desarrollo del _software_.
* Generación de código a partir de especificaciones de nivel muy alto.
* Se pueden hacer aplicaciones sin ser experto en el lenguaje.
* Tienen un conjunto de instrucciones limitadas.
* Y específicas del producto que las ofrece.

Por lo que suelen ser aplicaciones destinadas a negocios o manejo de bases de datos.

### Lenguajes de quinta generación

Son lenguajes destinados a ofrecer soluciones a los problemas suscitados por la investigación de inteligencia artificial o los sistemas expertos.
Se caracterizan no por ejecutar instrucciones, sino por anticipar las necesidades del usuario. Todavía se encuentran en desarrollo.