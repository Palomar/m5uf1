## 1.2 Código fuente, código objeto y código ejecutable; máquinas virtuales.

### Código fuente

El **código fuente** es aquel conjunto de órdenes secuenciales escritas en uno o más ficheros de texto bajo un lenguaje de programación, el cual permite al ser humano comunicarse con la máquina.
Dicho **código fuente** destaca en función del lenguaje de programación por ser más próximo a la máquina o por el contrario por ser más próximo al ser humano.
En función de esos dos extremos, el **código fuente** puede estar escrito en lenguajes de programación de **bajo nivel** (próximo al lenguaje máquina), o bien en un **alto nivel** (próximo al lenguaje humano).

Descrito lo anterior se puede afirmar pues que el **código fuente** no contiene el lenguaje que la máquina entiende, pues ésta se limita a un lenguaje que solamente la CPU entenderá en binario de acuerdo a su arquitectura.
Por esa razón para facilitar la tarea de programación es necesario un _lenguaje de alto nivel_ más accesible para los seres humanos.

Para que la máquina comprenda el **código fuente** por otro lado, se requiere de todo un procedimiento que permita traducir dicho _lenguaje de alto nivel_ empleado en el **código fuente** como _lenguaje máquina_, así que para ello se debe **compilar**.

### Código objeto

El **proceso de compilación** es la traducción del **codigo fuente** de los ficheros para convertirlos en binarios aptos para el procesador de la máquina, si encuentra errores en el *código fuente* nos avisará.
El contenido de estos ficheros compilados se le llama el **código objeto**, el cual no puede ejecutarse pero es la traducción a lenguaje máquina del **código fuente**.

### Código ejecutable

Es el código traducido completamente por el **enlazador** (_linker_) a partir del **código objeto** que permite ser interpretado directamente por la máquina.
El **enlazador** se encarga de insertar en dicho **código objeto** aquellas funciones llamadas de otra biblioteca generando el **código ejecutable** final.

### Máquina virtual

Surge como respuesta a las dificultades que pueden plantear el proceso de compilación para máquinas distintas. Puesto que la compilación consta de dos fases:

* Al traducir el código fuente al lenguaje intermedio.
* Al traducir dicho lenguaje intermedio al lenguaje de la máquina.

Así surge la idea de la máquina virtual como proceso intérprete para ese lenguaje intermedio, apto para interpretar el lenguaje intermedio en cualquier máquina por diferente que sea con tan sólo disponer de una _build_ concreta (Windows, Linux, MAC...).

Un ejemplo de máquina virtual es **JVM** empleada para interpretar las intrucciones de los bytecodes de **Java**, donde el programador emplea el lenguaje para Java que luego se compilarán al lenguaje intermedio que la máquina virtual JVM se encargará de interpretar según la versión en la máquina que se esté ejecutando.
