## 1.5 Características de los lenguajes más divulgados

Existe una gran variedad de **lenguajes de programación** y muchas veces muy distintos entre sí debido a la tecnología.
Por ello se habla de lenguaje muy divulgado cuando éste se usa mucho dentro de su ámbito. En el ámbito educativo se considera un lenguaje muy divulgado cuando se usa en las universidades y centros educativos para la docencia y la iniciación a la programación.

### Características de la programación estructurada

Se basa en el **teorema de la estructura** y, por tanto, prescinde de las intrucciones de transferencia incondicial, solamente se usa:

* **Secuencias**: instrucciones ejecutadas una detrás de otra. (A -> B)
* **Selecciones**: instrucción condicional con doble alternativa. (Si A -> B, sino C)
* **Iteraciones**: instrucción de bucle siempre que se da la condición. (Mientras A -> B)

Características:

* **Claridad**: el código debe ser claro y comprensible con cuantos comentarios, nombres de variables, etc.
Todo programa puede leerse de principio a fin sin interrumpir su flujo de abajo a arriba.

* **Teorema de la estructura**: Con tan sólo las 3 estructuras mencionadas arriba se puede escribir el programa.

* **Diseño descendente**: se trata de la abstracción por la cual un problema es dividido en pequeños problemas más fáciles de resolver y que conjuntamente resuelven el problema general.
Se basa en el dicho latín "dividi et impera".

* **Programación modular**: es la división del programa en pequeños programas, a raíz del diseño descendente, haciendo más manejable y comprensible el programa, facilitando la depuración de errores.
En la mayoría de lenguajes la modulación se traduce en: _procedimientos_ que son subprogramas que tras una tarea retornan 0 u otros valores que sirven para estructurar; y _funciones_ que realizan una tarea y retornan un único resultado o valor, usadas para crear operaciones nuevas que no tiene el lenguaje.

* **Tipos abstractos de datos**: los _tipos de datos_ son los valores capaces de ser soportados por una variable. Y a su vez una variedad limitada de operaciones que pueden hacer.
Dependiendo del lenguaje de programación se soportan distintos tipos de datos. Los booleans, enteros, reales, caracteres, etc. son los _tipos de datos básicos_.
Debido a los problemas que suscitaba esta limitación, surgieron los llamados _tipos de datos abstractos_ donde el programador define de qué tipo es y qué valores soporta.

### Características de la programación orientada a objetos

Debido a la limitación de las funciones y procedimientos de la programación estructurada (un cambio afectaba a todo el código, imposibilidad de saber qué funciones/procedimientos se requiere al comenzar con una aplicación, difícil reutilización del código) dio paso a la orientación a objetos.
La **orientación a objetos** es un paradigma de construcción de programas basado en la abstracción del mundo real.
Son **objetos** y no funciones o procedimientos dicha abstracción. Es la combinación de datos (atributos) y de métodos (funciones y procedimientos) los cuales nos permiten interactuar con ellos.
Por tanto un programa es conjunto de objetos que interactuan entre sí por medio de mensajes (llamadas a métodos).

Así un **lenguaje de programación orientado a objetos** (POO) son aquellos que adaptan con más o menos fidelidad el paradigma de orientación a objetos (OO).

Se basan en la integración de 5 conceptos:

* **Abstracción**: por medio de ésta se separan las características importantes de las que no lo son de un objeto del mundo real, es decir sus atributos y comportamientos.
De dicha abstracción se encargará la **clase**, una descripción genérica de un grupo de objetos que comparten características comunes.

* **Encapsulación**: permite escoger qué información es pública y qué se oculta respecto de los otros objetos. Pueden ser: 
    * Público: cualquier clase puede acceder a cualquier atributo o método declarado como público y utilizarlo.
    * Protegido: cualquier clase heredada puede acceder al atributo o método declarado como protegido en la clase madre y utilizarlo.
    * Privado: ninguna clase puede acceder a un atributo o método declarado como privado ni utilizarlo.

* **Modularidad**: si una aplicación puede dividirse en módulos separados, por lo general clases, y los módulos se pueden compilar y modificar sin afectar los demás, significa que se está usando un lenguaje programación que permite la modularidad.

* **Jerarquía**: permite la ordenación de las abstracciones. Son dos:
 * Herencia: aprovecha una clase anterior y simplemente emplea lo que es diferente en ella, especializando su comportamiento y reaprovechando código.
 * Agregación: es un objeto que se compone de otros objetos o componentes, es decir es una clase que se forma a partir de otro número X de clases sin las cuales no tiene sentido.

* **Polimorfismo**: permite dar diferentes formas a un método, sea en su definición o en su implementación.
Existe la sobrecarga (_overload_) de métodos cuando se implemente un mismo método en repetidas veces pero con parámetros distintos, de forma que el compilador escoge el método en función de los parámetros.
También está la sobrescritura (_override_) que consiste en implementar un método de una superclase con la misma definición (lo que incluye nombre, parámetros y valor de retorno). Por lo que puede ser diferente en las clases derivadas, implementándose de forma diferente o definirse de forma distinta.

