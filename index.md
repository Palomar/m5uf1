[1.1 Concepto de programa informático.](programa_informatic.md)

[1.2 Código fuente, código objeto y código ejecutable; máquinas virtuales.](codi_font.md)

[1.3 Tipos de lenguajes de programación.](tipus.md)

[1.4 Paradigmas de programación.](paradigmes.md)

[1.5 Características de los lenguajes más divulgados.](difosos.md)

[1.6 Fases del desarrollo de una aplicación: análisis, diseño, codificación, pruebas, documentación, mantenimiento y explotación, entre otros.](fases.md)

1.7 Proceso de obtención del código ejecutable a partir del código fuente; herramientas implicadas.