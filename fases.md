## 1.6 Fases del desarrollo de una aplicación: análisis, diseño, codificación, pruebas, documentación, mantenimiento y explotación, entre otros.

Se han desarrollado múltiples pautas sobre cómo desarrollar una aplicación. Una de ellas es la llamada **Métrica 3.0** del Ministerio de Administraciones Públicas.
Dicha metodologia divide en 5 fases el desarrollo que se siguen de forma secuencial. También es importante el rol que tienen los miembros del desarrollo, según la mencionada metodologia son:

* Partes interesadas (_stakeholders_).
* Jefe de Proyecto
* Consultores
* Analistas
* Programadores

### Estudio de la viabilidad

Es la primera parte del proceso y consiste en el análisis de las necesidades con la finalidad de ofrecer soluciones a corto plazo.
En ello se tienen en cuenta aspectos técnicos, económicos, legales y operativos.

Los **resultados** que arroje esta primera parte del proceso, determinaran si se procede a la siguiente fase y se continua con el proceso o por el contrario, si se decide abortar por inviable.

### Análisis del sistema de información

La finalidad de esta fase consiste en **concretar** qué requisitos requiere el usuario con la finalidad de moldear el sistema de información a obtener.
Por tanto gracias a la información de la primera fase, se inicia ese análisis para configurar en un lenguaje de alto nivel cómo será la futura aplicación de acuerdo a lo que se pide o requiere.
A su vez también se valoran requisitos no funcionales del sistema de información ya que se analizarán cuáles son sus límites, qué restricciones tendrá, bajo qué requisitos funcionará, etc.

### Diseño del sistema de información

Con esta fase se diseña la arquitectura del sistema y el entorno tecnológico que le dará soporte, además de especificar los detalles de los componentes del sistema.
Las especificaciones del sistema serán:

* Los componentes del sistema (clases si es POO; módulos si es PE).
* Los procedimientos de migración y componentes asociados.
* Definición y revisión del plan de pruebas, y el diseño de las verificaciones de los niveles de prueba establecidos.
* El catálogo de excepciones que se ocupará de las verificaciones relacionadas con el diseño o la arquitectura.
* Especificación de los requisitos de implantación.

### Construcción o codificación del sistema de información.

A partir del conjunto de especificaciones realizado en la etapa anterior, se procede a **construir/crear/codificar el sistema de información** siguiendo el diseño establecido, y a poner a prueba sus componentes.
Se desarrollan los procedimientos de operación y seguridad, además de desarrollar la documentación pertinente cuando proceda.
Durante esta fase se construyen los componentes de migración, de sus procedimientos y de su carga inicial de datos.

### Implantación del sistema

Una vez revisada la estrategia de implantación, se establece el plan de implantación y se detalla el equipo que lo llevará a término.
El sistema se someterá a las pruebas de implantación en colaboración con el usuario operador con el fin de someter el sistema a un test de condiciones extremas.

### Aceptación del sistema

El sistema se someterá a pruebas de aceptación llevadas a cabo por el usuario final. En dicha fase se llevará un plan de mantenimiento de forma que el responsable se familiarice antes de pasar a producción.
A su vez también sobre qué extremos se dará soporte y gestión una vez se pase a producción.